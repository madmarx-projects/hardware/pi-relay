# MadmaPi

## Intro

MadmaPi is a little piece of software used to control a RaspberryPi's relay through http requests.

## Usage

Press the buttons to activate/deactivate electricity through relays.

1. Hard drive disk (2To)
1. Bluetooth module
1. Monitor
1. Lights

# Installation

## Dependencies

Run npm command to install all the dependencies.

``` shell
npm i
```

# Working on the project

Run the server.

``` shell
gulp serve
```

or

``` shell
npm start
```

This will update automatically when a .js or .scss file is modified.
