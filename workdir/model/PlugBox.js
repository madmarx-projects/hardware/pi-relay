const wpi         = require('wiring-pi');
const Settings    = require('../settings');

module.exports = new class PlugBox {

  constructor() {
    this.switches = Settings.switches;
    this.leds = Settings.leds;

    this.resetPins();
    this.printAll();
  }

  /**
   * Reads the value of index position on the board
   * Example: (wpi.read(11) ? 'high' : 'low')
   * @param {integer} index 1, 2, 3 or 4. This is the position of the power plug
   * @returns {boolean} state at index of LEDs array
   */
  read(index) {
    index--; // array compliant
    try {
      return wpi.read(this.leds[index]);
    } catch (e) {
      console.error("Wrong powerplug position: " + index, e);
    }
  }


  /**
   * Sets the index powerplug to up
   * @param {integer} index 1, 2, 3 or 4. This is the position of the power plug.
   * @returns {boolean} state at index of LEDs array
   */
  switchOn(index) {
    console.log('switch ' + index + ' on...');

    index--; // array compliant
    try {
      wpi.write(this.leds[index], wpi.HIGH);
      // return wpi.write(this.switches[index], wpi.HIGH);
      wpi.mode(this.switches[index], wpi.OUTPUT);
      wpi.write(this.switches[index], wpi.HIGH);
      return this.read(index++);
    } catch (e) {
      console.error("Wrong powerplug position: " + index, e);
    }
  }


  /**
   * Sets the index powerplug to down
   * @param {integer} index 1, 2, 3 or 4. This is the position of the power plug.
   * @returns {boolean} state at index of LEDs array
   */
  switchOff(index) {
    console.log('switch ' + index + ' off...');

    index--; // array compliant
    try {
      wpi.write(this.leds[index], wpi.LOW);
      // return wpi.write(this.switches[index], wpi.LOW);
      wpi.mode(this.switches[index], wpi.INPUT);
      return this.read(index++);
    } catch (e) {
      console.error("Wrong powerplug position: " + index, e);
    }
  }


  /**
   * Prints states of the power plugs and their LEDs
   */
  printAll() {
    let str = 'SWITCHES  LEDS\n';
    for (let i = 0; i < this.switches.length; ++i) {
      str += (' [' + wpi.read(this.switches[i]) + ']     ');
      str += (' [' + wpi.read(this.leds[i]) + ']\n');
    }

    console.log(str);

    return str;
  }


  resetPins() {
    wpi.init({
      gpiomem: true,
      mapping: 'physical',
      mock: undefined
    });

    for (let i = 0; i < this.switches.length; ++i) {
      wpi.open(this.switches[i], wpi.INPUT);
      // wpi.open(this.switches[i], wpi.OUTPUT, wpi.LOW);
      wpi.open(this.leds[i], wpi.OUTPUT, wpi.LOW);
    }

  }

}();
