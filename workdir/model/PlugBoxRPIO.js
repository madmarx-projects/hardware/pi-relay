const rpio      = require('rpio');
const Settings  = require('../settings');

module.exports = new class PlugBox {

  constructor() {
    this.switches = Settings.switches;
    this.leds = Settings.leds;

    this.resetPins();
    this.printAll();
  }


  /**
   * Reads the value of index position on the board
   * Example: (rpio.read(11) ? 'high' : 'low')
   * 
   * @param {integer} index 1, 2, 3 or 4. This is the position of the power plug
   * @returns {boolean} state at index of LEDs array
   */
  read(index) {
    index--; // array compliant
    try {
      return rpio.read(this.leds[index]);
    } catch (e) {
      console.error("Wrong powerplug position: " + index, e);
    }
  }


  /**
   * Sets the index powerplug to up
   * 
   * @param {integer} index 1, 2, 3 or 4. This is the position of the power plug.
   * @returns {boolean} state at index of LEDs array
   */
  switchOn(index) {
    console.log('switch ' + index + ' on...');

    index--; // array compliant
    try {
      rpio.write(this.leds[index], rpio.HIGH);
      // return rpio.write(this.switches[index], rpio.HIGH);
      rpio.mode(this.switches[index], rpio.OUTPUT);
      rpio.write(this.switches[index], rpio.HIGH);
      return this.read(++index);
    } catch (e) {
      console.error("Wrong powerplug position: " + index, e);
    }
  }


  /**
   * Sets the index powerplug to down
   * 
   * @param {integer} index 1, 2, 3 or 4. This is the position of the power plug.
   * @returns {boolean} state at index of LEDs array
   */
  switchOff(index) {
    console.log('switch ' + index + ' off...');

    index--; // array compliant
    try {
      rpio.write(this.leds[index], rpio.LOW);
      // return rpio.write(this.switches[index], rpio.LOW);
      rpio.mode(this.switches[index], rpio.INPUT);
      return this.read(++index);
    } catch (e) {
      console.error("Wrong powerplug position: " + index, e);
    }
  }


  /**
   * Prints states of the power plugs and their LEDs
   */
  printAll() {
    let str = 'SWITCHES  LEDS\n';
    for (let i = 0; i < this.switches.length; ++i) {
      str += (' [' + rpio.read(this.switches[i]) + ']     ');
      str += (' [' + rpio.read(this.leds[i]) + ']\n');
    }

    console.log(str);
    return str;
  }


  resetPins() {
    rpio.init({
      gpiomem: true,
      mapping: 'physical',
      mock: undefined
    });

    for (let i = 0; i < this.switches.length; ++i) {
      rpio.open(this.switches[i], rpio.INPUT);
      // rpio.open(this.switches[i], rpio.OUTPUT, rpio.LOW);
      rpio.open(this.leds[i], rpio.OUTPUT, rpio.LOW);
    }

  }

}();

