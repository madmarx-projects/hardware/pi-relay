const express       = require('express');
const router        = express.Router();
const bodyParser    = require('body-parser');
const cookieParser  = require('cookie-parser');
const path          = require('path');
const favicon       = require('serve-favicon');
const parseurl      = require('parseurl');
const handlebars    = require('express-handlebars').create({ defaultLayout: 'skeleton' });
const execFile      = require('child_process').exec; // execFile pour async
const os            = require('os');

const Switch        = require('./routes/switch');

const app           = express();


app.disable('x-powered-by');

// VIEW ENGINE SETUP
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');


// SETUP
app.use(favicon(path.join(__dirname, 'assets/img/favicon.png')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, '../build')));


// ROUTING
app.use('/switch', Switch);

app.use('/', (req, res, next) => {
  res.status(200).send('Bienvenue.');
});


app.use((err, req, res, next) => {
  console.error('An error has occured', err);
});

module.exports = app;
