const express   = require('express');
const router    = express.Router();
const plugBox   = require('../model/PlugBoxRPIO');

const Settings  = require('../settings');

// ROUTE /switch/:id/*
// :id = 1, 2, 3 ou 4

router.use('/:id/toggle', (req, res, next) => {
  let state;

  try {
    if (plugBox.read(req.params.id)) {
      state = plugBox.switchOff(req.params.id);
    } else {
      state = plugBox.switchOn(req.params.id);
    }
  } catch (e) {
    console.error('id is not a number, was: ' + (req.params.id));
  }

  plugBox.printAll();

  res.status(200).send(
    {
      "pin": req.params.id,
      "state": state
    }
  );
});


router.use('/:id/on', (req, res, next) => {
  let state;

  try {
    if (!plugBox.read(req.params.id)) {
      state = plugBox.switchOn(req.params.id);
    } else {
      console.log((req.params.id) + ' already up.');
    }
  } catch (e) {
    console.error('id is not a number, was: ' + (req.params.id));
  }

  plugBox.printAll();

  res.status(200).send(
    {
      "pin": req.params.id,
      "state": state
    }
  );
});


router.use('/:id/off', (req, res, next) => {
  let state;

  try {
    if (plugBox.read(req.params.id)) {
      state = plugBox.switchOff(req.params.id);
    } else {
      console.log((req.params.id) + ' already down.');
    }
  } catch (e) {
    console.error('id is not a number, was: ' + (req.params.id));
  }

  plugBox.printAll();

  res.status(200).send(
    {
      "pin": req.params.id,
      "state": state
    }
  );
});

router.use('/', (req, res, next) => {
  res.status(200).send(plugBox.printAll());
});

module.exports = router;
