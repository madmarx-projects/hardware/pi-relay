//ENVIRONMENT
exports.env = env = 'dev'; // 'dev' | 'test' | 'prod'

// WEB SERVER
exports.servers = servers = {
  dev: {
    domain: 'http://localhost:80/',
    host: 'localhost',
    port: 80
  },
  test: {
    domain: 'http://localhost:80/',
    host: 'localhost',
    port: 80
  },
  prod: {
    domain: 'http://madmarx.servehttp.com:1337/',
    host: 'madmarx.servehttp.com',
    port: 80
  }
};
exports.currentServer = servers[env];

exports.switchesWiringPi  = [7, 0, 2, 3]; // pins wiringPi
exports.switches          = [7, 11, 13, 15]; // pins physical

exports.ledsWiringPi      = [13, 6, 10, 11]; // pins wiringPi
exports.leds              = [21, 22, 24, 26]; // pins physical
