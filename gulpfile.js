const gulp          = require('gulp');
const sass          = require('gulp-sass');
const concat        = require('gulp-concat');
const gulpif        = require('gulp-if');
const autoprefixer  = require('gulp-autoprefixer');
const minify_css    = require('gulp-clean-css');
const minify_js     = require('gulp-minify');
const rename        = require('gulp-rename');
const webserver     = require('gulp-webserver');
const babelify      = require('babelify');
const source        = require('vinyl-source-stream');
const buffer        = require('vinyl-buffer');
const browserify    = require('browserify');


var RELEASE = true; //! (process.env.DEBUG == "True")


gulp.task('buildall', ['compile:sass', 'compile:js', 'compile:app', 'compile:bundleJs', 'copy']);
gulp.task('compile:sass', () => {
  return gulp.src('workdir/assets/css/**/*.scss')
    .pipe(sass())
    .on('error', function (err) {
      console.log(err);
      this.emit('end');
    })
    .pipe(concat('style.css'))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
    }))
    .pipe(gulpif(RELEASE, minify_css()))
    .pipe(gulpif(RELEASE, rename('style.min.css')))
    .pipe(gulp.dest('public/css'));
});
gulp.task('compile:js', () => {
  return gulp.src('workdir/assets/js/**/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulpif(RELEASE, minify_js({
      ext: {
        min: '.min.js'
      },
      noSource: true
    })))
    .on('error', function (err) {
      console.log(err);
      this.emit('end');
    })
    .pipe(gulp.dest('public/js/'));
});
gulp.task('compile:bundleJs', function () {
  return browserify({ entries: paths.jsentries, debug: !RELEASE })
    //.transform(babelify.configure({presets: ['es2015']}))
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(gulp.dest('public/js'));
});
// C'est pas un peu de la merde ça?
gulp.task('compile:app', () => {
  return gulp.src('workdir/app.js')
    //.transform(babelify.configure({presets: ['es2015']}))
    .pipe(gulpif(RELEASE, minify_js({
      ext: {
        min: '.min.js'
      },
      noSource: true
    })))
    .on('error', function (err) {
      console.log(err);
      this.emit('end');
    })
    .pipe(gulp.dest('public/'));
});

gulp.task('copy', ['copy:img', 'copy:views', 'copy:libs']);
gulp.task('copy:img', () => {
  return gulp.src('workdir/assets/img/**/*')
    .pipe(gulp.dest('public/assets/img/'));
});
gulp.task('copy:views', () => {
  return gulp.src('workdir/views/**/*.twig')
    .pipe(gulp.dest('public/views/'));
});
gulp.task('copy:libs', () => {
  return gulp.src('workdir/libs/**/*')
    .pipe(gulp.dest('public/libs'));
});


gulp.task('watch', ['buildall', 'watch:sass', 'watch:app', 'watch:js', 'watch:views']);
gulp.task('watch:sass', () => {
  return gulp.watch('workdir/assets/css/**/*.scss', ['compile:sass']);
});
gulp.task('watch:js', () => {
  return gulp.watch('workdir/assets/js/**/*.js', ['compile:js']);
});
gulp.task('watch:app', () => {
  return gulp.watch('workdir/app.js', ['compile:app']);
});
gulp.task('watch:views', () => {
  return gulp.watch('workdir/views/**/*.twig', ['copy:views']);
});

gulp.task('default', ['buildall']);

gulp.task('serve', ['watch'], () => {
  return gulp.src('public')
    .pipe(webserver({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});


var paths = {
  js: [
    // 'bower_components/babel-polyfill/browser-polyfill.js',
    'bower_components/jquery/dist/jquery.min.js',
    //'bower_components/what-input/what-input.min.js',
    //'bower_components/foundation-sites/dist/foundation.min.js',
    //'bower_components/angular/angular.js',
    //'bower_components/angular-resource/angular-resource.min.js'
  ],
  jsentries: [
    'workdir/assets/js/index.js' /////////////////////////////////////////
  ],
  img: 'workdir/assets/img/**/*'
};
